#!/usr/bin/python2.7
#
# Raspberry pi predenting to be a bluetooth keyboard.
# This version sends the hex after recieving 14 digits from the barcode scanner. It also times out if it recieves no data from the keyboard for 0.5 sec
# 


import os # Used to call external commands
import sys # Used to exit the script
import bluetooth
from bluetooth import *
import dbus # Used to set up the SDP record
import time # Used for pausing the process
import evdev # Used to get input from the keyboard
from evdev import *
import keymap # Used to map evdev input to hid keycodes

class Bluetooth:
    HOST = 0 # BT Mac address
    PORT = 1 # Bluetooth Port Number

    # Define the ports we'll use
    P_CTRL = 17
    P_INTR = 19

    def __init__(self):
        # Set the device class to a keyboard and set the name
        os.system("hciconfig hci0 class 0x002540")
        os.system("hciconfig hci0 name Raspberry\ Pi")
        # Make device discoverable
        os.system("hciconfig hci0 piscan")

        # Define our two server sockets for communication
        self.scontrol = BluetoothSocket(L2CAP)
        self.sinterrupt = BluetoothSocket(L2CAP)

        # Bind these sockets to a port
        self.scontrol.bind(("", Bluetooth.P_CTRL))
        self.sinterrupt.bind(("", Bluetooth.P_INTR))
        # Set up dbus for advertising the service record
        self.bus = dbus.SystemBus()
        try:
            self.manager = dbus.Interface(self.bus.get_object("org.bluez", "/"),
                                                              "org.bluez.Manager")
            adapter_path = self.manager.DefaultAdapter()
            self.service = dbus.Interface(self.bus.get_object("org.bluez", adapter_path),
                                                              "org.bluez.Service")
        except:
            sys.exit("Could not configure bluetooth. Is bluetoothd started?")

        # Read the service record from file
        try:
            fh = open(sys.path[0] + "/sdp_record.xml", "r") # open the file service record as read only
        except:
            sys.exit("Could not open the sdp record. Exiting...")            
        self.service_record = fh.read()  # read the file
        fh.close() # close the file 

    def listen(self):
        # Advertise our service record
        self.service_handle = self.service.AddRecord(self.service_record)
        print "Service record added"

        # Start listening on the server sockets
        self.scontrol.listen(1) # Limit of 1 connection
        self.sinterrupt.listen(1)
        print "Waiting for a connection"
        self.ccontrol, self.cinfo = self.scontrol.accept()
        print "Got a connection on the control channel from " + self.cinfo[Bluetooth.HOST]
        self.cinterrupt, self.cinfo = self.sinterrupt.accept()
        print "Got a connection on the interrupt channel from " + self.cinfo[Bluetooth.HOST]

    def send_input(self, ir):
        # Convert the hex array to a string
        hex_str = ""
        for element in ir:
            if type(element) is list:
                # This is our bit array - convert it to a single byte represented
                # as a char
                bin_str = ""
                for bit in element:
                    bin_str += str(bit)
                hex_str += chr(int(bin_str, 2))
            else:
                # This is a hex value - we can convert it straight to a char
                hex_str += chr(element)
        # Send an input report
        # print int(hex_str)
        self.cinterrupt.send(hex_str)


class Keyboard():
    def __init__(self):
        # The structure for an bt keyboard input report (size is 10 bytes)
        self.state = [ 
               0xA1, # This is an input report
               0x01, # Usage report = Keyboard
               # Bit array for Modifier keys (D7 being the first element, D0 being last)
               [0,   # Right GUI - (usually the Windows key) 
                0,   # Right ALT
                0,   # Right Shift
                0,   # Right Control
                0,   # Left GUI - (again, usually the Windows key)
                0,   # Left ALT
                0,   # Left Shift
                0],  # Left Control
               0x00, # Vendor reserved
               0x00, # Rest is space for 6 keys 
               0x00,
               0x00,
               0x00,
               0x00,
               0x00 ]
        self.send_ready = 0
        self.number = []
        self.time = 0
        # Keep trying to get a keyboard
        have_dev = False
        while have_dev == False:
            try:
                # Try and get a keyboard - should always be event0 as we're only
                # plugging one thing in
                self.dev = InputDevice("/dev/input/event0")
                have_dev = True
            except OSError:
                print "Keyboard not found, waiting 3 seconds and retrying"
                time.sleep(3)

        print "Found a keyboard"

    def change_state(self, event):
        evdev_code = ecodes.KEY[event.code]   # evdev_code = input from USB keyboard
        if event.value == 1:
            if (time.time() - self.time) >= 0.5:
                self.number = []
            if ord(evdev_code[-1]) <= 57 and ord(evdev_code[-1]) >= 48: # if the ascii code is between 57 and 48 (numbers between 0 and 9)
                self.number = self.number +  [evdev_code[-1]]
                self.time = time.time() # reset the timer because data has been recieved 
            if len(self.number) >= 14:
                self.send_ready = 1

    def event_loop(self, bt):
        for event in self.dev.read_loop():
            # Only bother if we press a key and it's an up or down event
            if event.type == ecodes.EV_KEY and event.value < 2:
                    self.change_state(event)
                    if self.send_ready == 1: # if it has recieved 14 characters
                        self.send_ready = 0
                        dec = int(''.join(map(str,self.number)))   # convert the list to an int
                        hex_no = hex(dec).rstrip("L")       # convert to hex. L appears here if it is a 'long', not 'int'
                        self.number = list(hex_no[2:])     # converts the int to a list and removes the 0x
                        if len(self.number) % 2 == 0:      # if self.number has an even number of numbers then
                            a = "".join(self.number)       # make it in to a sting
                        else:                              # otherwise 
                            a = "0" + "".join(self.number) # make it in to a string and add a 0 on the front

                        b = "".join(reversed([a[i:i+2] for i in range(0, len(a), 2)])) # reverse the bytes
                        self.number = list(b)   # convert to a list
                        for j in range (0,len(self.number)):
                            self.state[4] = keymap.convert("KEY_" + str(self.number[j]).upper())
                            bt.send_input(self.state)
                            self.state = [161, 1, [0, 0, 0, 0, 0, 0, 0, 0], 0, 0, 0, 0, 0, 0, 0] # send a row of blanks so it knows you aren't holding down keys
                            bt.send_input(self.state)
                            j = j + 1
                        self.state[4] = 40 # prepare to send an 'enter'
                        bt.send_input(self.state) #send via bluetooth
                        self.state = [161, 1, [0, 0, 0, 0, 0, 0, 0, 0], 0, 0, 0, 0, 0, 0, 0] # prepare a row of blanks
                        bt.send_input(self.state) # send via bluetooth
                        self.number = []   # reset self.number


if __name__ == "__main__":
    # We can only run as root
    if not os.geteuid() == 0:
        sys.exit("Only root can run this script")

    bt = Bluetooth()
    bt.listen()
    kb = Keyboard()
    kb.event_loop(bt)
